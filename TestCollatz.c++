// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ------------
// cycle_length
// ------------

TEST(CollatzFixture, cycle_length_1) {
    const long a = 3;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, cycle_length_2) {
    const long a = 5;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 6);
}

TEST(CollatzFixture, cycle_length_3) {
    const long a = 1;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, cycle_length_4) {
    const long a = 999999;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 259);
}

TEST(CollatzFixture, cycle_length_5) {
    const long a = 1000;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 112);
}

TEST(CollatzFixture, cycle_length_6) {
    const long a = 5001;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 65);
}

TEST(CollatzFixture, cycle_length_7) {
    const long a = 999;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 50);
}

TEST(CollatzFixture, cycle_length_8) {
    const long a = 1999;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 51);
}

TEST(CollatzFixture, cycle_length_9) {
    const long a = 9;
    const long v = cycle_length(a);
    ASSERT_EQ(v, 20);
}


// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(1, 3);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(999999, 1000000);
    ASSERT_EQ(v, 259);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(2, 1234);
    ASSERT_EQ(v, 182);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(1000, 2000);
    ASSERT_EQ(v, 182);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(999, 1001);
    ASSERT_EQ(v, 143);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(500, 1000);
    ASSERT_EQ(v, 179);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
