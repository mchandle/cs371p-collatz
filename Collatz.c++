// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <unordered_map>
#include "Collatz.h"
#include "meta.cpp"
#include <assert.h>

using namespace std;

// ------------
// collatz_read
// ------------
unordered_map<int, long> lazyCache;

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

long cycle_length(long i) {
    long result;
    //check if the key does not exists
    if (lazyCache.find(i) == lazyCache.end()) {
        if (i == 1)
            return 1;
        long nextVal;
        if (i % 2 == 0) {
            nextVal = i >> 1;
            result = cycle_length(nextVal);
            lazyCache[nextVal] = result;
            return 1 + result;
        }
        nextVal = (i + (i >> 1) + 1);
        result = cycle_length(nextVal);
        lazyCache[nextVal] = result;
        return 2 + result;
    }
    return lazyCache[i];
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    //switch parameters if i < j
    if (i > j) {
        int copy = j;
        j = i;
        i = copy;
    }
    assert (i <= j);
    long maxCycleLength = 0;
    long cycleLength = 0;

    long m = (j / 2) + 1;
    if (i < m) {
        i = m;
    }
    // if i starts somehwere between thousands
    // loop from i to closest thousand
    while (i % 1000 != 0 && i <= j) {
        if (lazyCache.find(i) == lazyCache.end()) {
            cycleLength = cycle_length((long) i);
            lazyCache[i] = cycleLength;
        }
        cycleLength = lazyCache[i];
        if (cycleLength > maxCycleLength) {
            maxCycleLength = cycleLength;
        }
        ++i;

    }
    if (j - i >= 1000) { // i is a factor of 1000
        //there is a range of atleast a thousand between i and j
        assert(sizeof(meta) == 1000);
        while (i < (j / 1000) * 1000) {
            cycleLength = meta[i / 1000];
            if (cycleLength > maxCycleLength) {
                maxCycleLength = cycleLength;
            }
            i += 1000;
        }
    }
    assert(j - i < 1000);

    while (i <= j) {
        if (lazyCache.find(i) == lazyCache.end()) {
            cycleLength = cycle_length((long) i);
            lazyCache[i] = cycleLength;
        }
        cycleLength = lazyCache[i];
        if (cycleLength > maxCycleLength) {
            maxCycleLength = cycleLength;
        }
        ++i;
    }
    return maxCycleLength;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
